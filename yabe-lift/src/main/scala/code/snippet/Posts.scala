package code.snippet

import java.util.Date
import scala.xml.NodeSeq.seqToNodeSeq
import scala.xml.NodeSeq
import scala.xml.Unparsed
import code.comet.PostsServer
import code.lib.MapperBinder.bindMapper
import code.lib.YabeHelper
import code.model.Post
import code.model.User
import net.liftweb.common.Full
import net.liftweb.http.NodeSeqFuncOrSeqNodeSeqFunc.promoteNodeSeq
import net.liftweb.http.js.jquery.JqJsCmds.AppendHtml
import net.liftweb.http.PaginatorSnippet
import net.liftweb.http.S
import net.liftweb.http.SHtml
import net.liftweb.mapper.MappedField.mapToType
import net.liftweb.mapper.Ascending
import net.liftweb.mapper.By
import net.liftweb.mapper.{ By_< => By_< }
import net.liftweb.mapper.{ By_> => By_> }
import net.liftweb.mapper.Descending
import net.liftweb.mapper.MaxRows
import net.liftweb.mapper.OrderBy
import net.liftweb.mapper.StartAt
import net.liftweb.util.AnyVar.whatVarIs
import net.liftweb.util.Helpers.strToCssBindPromoter
import net.liftweb.util.IterableConst.itNodeSeqFunc
import net.liftweb.util.StringPromotable.jsCmdToStrPromo
import net.liftweb.util.StringPromotable.longToStrPromo
import net.liftweb.util.CssSel
import net.liftweb.http.RequestVar
import net.liftweb.http.js.jquery.JqJsCmds.Hide
import scala.collection.immutable.List
import net.liftweb.http.js.jquery.JqJsCmds.Show
import net.liftweb.http.js.JsCmd

object Posts {
  val ITEMS_PER_PAGE = 8

  // just show all new posts added..
  def listOlder = {
    val latestPost = Post.find(OrderBy(Post.id, Descending))
    latestPost match {
      case Full(p) => listPage //listPage.filter(p.id != _.id)
      case _ => listPage
    }
  }

  def listByUser(userId: Long) = {
    listPageByUser(userId)
  }

  def listPage = {
    val posts = new Posts
    posts.page
  }

  def listPageByUser(userId: Long) = {
    val postByUser = new Posts
    postByUser.listPostByPagePerUser(userId)
  }

  def listPageByUser(page: Int, userId: Long) = {
    val postByUser = new Posts
    postByUser.listPostByPagePerUser(page, userId)
  }

  def listPostByPage(currentPage: Int) = {
    val postByPage = new Posts
    postByPage.listPostByPage(currentPage)
  }
}

class Posts extends PaginatorSnippet[Post] {

  object siteUri extends RequestVar(S.uri.toString())

  def initComet = {
    PostsServer ! "go on" //siteUri.toString() //postId.is
    "*" #> ""
  }

  def listLatest: CssSel = {
    val latestPost = Post.find(OrderBy(Post.id, Descending))
    //"*" #> bindMapper()

    latestPost match {
      case Full(p) => {
        "*" #> bindMapper(p, {
          ".post-comments *" #> (" | " + p.countComments + " comments " + p.latestCommentAuthor)
        }) _
      }
      case _ => "*" #> <span></span>
    }
  }

  override def itemsPerPage = Posts.ITEMS_PER_PAGE
  override def count = Post.count
  override def page = listPostByPage(curPage)

  def listPostByPage(currenPage: Int) = {
    Post.findAll(StartAt(currenPage * itemsPerPage), MaxRows(itemsPerPage), OrderBy(Post.postedAt, Descending), OrderBy(Post.id, Descending))
  }

  def listPostByPagePerUser(userId: Long): List[Post] = {
    listPostByPagePerUser(curPage, userId)
  }

  def listPostByPagePerUser(currentPage: Int, userId: Long) = {
    Post.findAll(By(Post.author, userId), StartAt(currentPage * itemsPerPage), MaxRows(itemsPerPage), OrderBy(Post.postedAt, Descending), OrderBy(Post.id, Descending))
  }

  def listOlder: CssSel = {
    val latestPost = Post.find(OrderBy(Post.postedAt, Descending))

    latestPost match {
      case Full(p) => {
        val olderPosts = page.filter(p.id != _.id)

        renderPostsList(olderPosts)
      }
      case _ => "*" #> ""
    }
  }

  def listPostsByTag: CssSel = {
    val posts = Post.getPostsByTag(S.param("tag") openOr "")

    renderPostsList(posts)
  }

  def getTag: CssSel = {
    "span" #> (S.param("tag") openOr "")
  }

  def titlePostsByTag: CssSel = {
    "*" #> <title>{ ("Posts tagged with " + (S.param("tag") openOr "")) }</title>
  }

  private def renderPostsList(posts: List[Post]): CssSel = {
    "*" #> posts.map {
      p =>
        "*" #> bindMapper(p, {
          ".post-comments *" #> (" | " + p.countComments + " comments " + p.latestCommentAuthor)
        }) _
    }
  }

  def title: CssSel = {
    val post = Post.find(By(Post.id, S.param("id").openTheBox.toLong))
    post match {
      case Full(p) => "*" #> <title>{ p.title }</title>
      case _ => "*" #> <title>invalid post</title>
    }
  }

  def read: CssSel = {
    val post = Post.find(By(Post.id, S.param("id").openTheBox.toLong))

    post match {
      case Full(p) => {
        "*" #> bindMapper(p, { ".post-tags *" #> Unparsed(p.showTagMetaStr) }) _
      }
      case _ => "*" #> ""
    }
  }

  def prev: CssSel = {
    val currPostId = S.param("id").openTheBox.toLong;
    val authorId = S.param("aId") openOr 0;

    val prevPost = {
      if (authorId != 0) {
        Post.find(By(Post.author, authorId.toString().toLong), OrderBy(Post.id, Descending),
          By_<(Post.id, currPostId))
      } else {
        Post.find(OrderBy(Post.id, Descending),
          By_<(Post.id, currPostId))
      }
    }

    prevPost match {
      case Full(p) => {
        var url = "/read/" + p.id.get
        if (authorId != 0)
          url = "/user/" + authorId + "/read/" + p.id.get
        "a [href]" #> url //&
        // "a *" #> p.title.get
      }
      case _ => "*" #> ""
    }
  }

  def next: CssSel = {
    val currPostId = S.param("id").openTheBox.toLong;
    val authorId = S.param("aId") openOr 0;

    val nextPost = {
      if (authorId != 0) {
        Post.find(By(Post.author, authorId.toString().toLong), OrderBy(Post.id, Ascending),
          By_>(Post.id, currPostId))
      } else {
        Post.find(OrderBy(Post.id, Ascending),
          By_>(Post.id, currPostId))
      }
    }

    nextPost match {
      case Full(p) => {
        var url = "/read/" + p.id.get
        if (authorId != 0)
          url = "/user/" + authorId + "/read/" + p.id.get

        "a [href]" #> url //&
        // "a *" #> p.title.get
      }
      case _ => "*" #> ""
    }
  }

  //Count the number of posts that posted by current user
  def countByUser: CssSel = {
    val userId = User.currentUserId.openTheBox
    val count = Post.count(By(Post.author, userId.toLong))
    "span" #> count
  }

  //list posts posted by this user
  def listByUser: CssSel = {
    val userId = User.currentUserId.openTheBox
    val posts = Post.findAll(By(Post.author, userId.toLong))

    var odd = "even"
    "*" #> posts.map { post =>  odd = YabeHelper.oddOrEven(odd)
        "p [class]" #> ("post " + odd) &
          "a [href]" #> ("/admin/posts/edit/" + post.id) &
          "a *" #> post.title
    }
  }

  def add: CssSel = {
    val post = Post.create
    def process() = {
      post.author.set(User.currentUserId.openTheBox.toInt)
      post.postedAt.set(new Date())
      post.validate match {
        case Nil => {
          post.save
          post.tags.setMultiple(post, S.param("tags").openTheBox)
          PostsServer ! "go on"
          S.redirectTo("/admin/posts/index")
        }
        case errors => S.error(errors)
      }
    }
    "#post-add" #> bindMapper(post) _ &
      "type=submit" #> SHtml.onSubmitUnit(() => process)
  }

  def edit: CssSel = {
    val id = S.param("id").openTheBox
    val post = Post.find(By(Post.id, id.toLong)).openTheBox

    def process() = {
      post.validate match {
        case Nil => {
          post.save
          S.redirectTo("/admin/posts/index")
        }
        case errors => S.error(errors)
      }
    }

    if (post.author.toLong != User.currentUserId.openTheBox.toLong) {
      "*" #> <span>Sorry, you do not have permission to edit this post in this page.</span>
    } else {
      "#post-edit" #> bindMapper(post) _ &
        "name=tags" #> SHtml.text(post.tags.concat, post.tags.setMultiple(post, _)) &
        "type=submit" #> SHtml.onSubmitUnit(process)
    }
  }

  def getUserName: CssSel = {
    val firstName = User.currentUser.openTheBox.firstName
    val lastName = User.currentUser.openTheBox.lastName

    "span" #> (firstName + " " + lastName)
  }

  /*  Show more list functionalities  */

  var nextPage = curPage + 1;

  /**
   *
   */
  def showMore: CssSel = {
    def process() = {
      Thread.sleep(400)
      val posts = listPostByPage(nextPage)
      nextPage += 1

      AppendHtml("showMoreList", showMorePostHTML(posts))
    }
    "#ajaxifyShowMore" #> SHtml.idMemoize(
      outer =>
        "#showMoreList *" #> "" &
          "#showMore [onclick]" #> SHtml.ajaxInvoke(process))
  }

  /**
   *
   */
  def showMorePostHTML(posts: List[Post]): NodeSeq = {
    posts.flatMap(p =>
      <div class="post teaser">
        <h2 class="post-title">
          <a class="post-title-link" href={ "/read/" + p.id }>{ p.title }</a>
        </h2>
        <div class="post-metadata">
          <span class="post-author">by <a class="link" href={ "/user/" + p.author.id }>{ p.author.first } { p.author.last }</a> </span>
          ,<span class="post-date"> { p.postedAt } </span>
          <span class="post-comments">&nbsp;|&nbsp; { p.countComments } comments{ p.latestCommentAuthor }</span>
        </div>
      </div>)
  }
}

/**
 *
 */
class ShowMore {

  var nextPage = 1 // should be nextPage = curPage + 1  
  val initial = Posts.listPostByPage(nextPage)

  def render = {
    def process() = {
      val nextnextPage = nextPage + 1;
      val posts = Posts.listPostByPage(nextPage)
      val nextPosts = Posts.listPostByPage(nextnextPage)
      Thread.sleep(400)
      nextPage += 1

      (if (nextPosts.length == 0) Hide("btnShowMore")
      else Show("btnShowMore")) &
        AppendHtml("showMoreList", showMoreHTML(posts))
    }

    if (initial.length == 0) "* " #> NodeSeq.Empty
    else
      "type=button" #> SHtml.ajaxButton("Show More", () => process)
  }

  def showMoreHTML(posts: List[Post]): NodeSeq = {
    posts.flatMap(p =>
      <div class="post teaser">
        <h2 class="post-title" style="margin-bottom: 10px;">
          <a class="post-title-link" style="text-decoration: none; color: #7A737A;" href={ "/read/" + p.id }>{ p.title }</a>
        </h2>
        <div class="post-footer" style="margin-top: 0px;">
          <div class="post-footer-line post-footer-line-1">
            <span class="post-author">
              Posted by{ p.author.first }{ p.author.last }
            </span><span class="date-header" style="float: right;">{ p.postedAt }</span>
          </div>
        </div>
      </div>)
  }

}