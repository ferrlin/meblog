package code.comet

import net.liftweb._
import http._
import actor._
import net.liftweb.mapper._
import net.liftweb.util._
import code.model._
import code.snippet.Posts

/**
 *
 */
object PostsServer extends LiftActor with ListenerManager {
  private var posts = List[Post]()

  def createUpdate = posts

  override def lowPriority = {
    //initial load of page
    case "go on" => {
      posts = Posts.listOlder      
      updateListeners()
    }
    case userId: Long => {
      posts = Posts.listPageByUser(userId) //Post.findAll(By(Post.author,userId))
      updateListeners()
    } 
  }
} 