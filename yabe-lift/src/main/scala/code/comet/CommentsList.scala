package code.comet

import scala.xml.Unparsed
import net.liftweb._
import http._
import util._
import Helpers._
import code.model._
import code.lib._
import common.{ Full, Logger }
import code.module.NamedCometActor
import code.module.CometListerner
import net.liftweb.actor.LiftActor
import net.liftweb.http.js.{ JsCmd, JsCmds }
import net.liftweb.mapper.By

class CommentsList extends CometActor with CometListener {

  override def lifespan = Full(120 seconds)

  private var comments = List[Comment]()
  def registerWith = CommentsServer

  override def lowPriority = {
    case c: List[Comment] => comments = c; reRender
  }

  def render = {
    ".comment-count *" #> (comments.length + " comments") &
      ".comment" #> comments.map {
        c =>
          ".comment-author-info *" #> (c.author.get match {
            case "" => "guest"
            case _ => c.author.get
          }) &
            ".comment-date *" #> YabeHelper.fmtDateStr(c.postedAt.get) &
            ".comment-content-span" #> Unparsed(c.content.get.replaceAll("\n", "<br />"))
      }
  }
}

case class Message(postId: String, comments: List[Comment])

/**
 * Another implementation to support solution using DispatcherActor.
 */
class CommentsList2 extends NamedCometActor {
  private var comments = List[Comment]()

  //override def defaultPrefix = Full("comet")

  // time out the comet actor if it hasn't been on a page for 2 minutes
  //override def lifespan = Full(120 seconds)

  //var showingVersion = ""

  override def lowPriority: PartialFunction[Any, Unit] = {
    case Message(postId, c) => {      
      comments = c;reRender(true)
      //this.comments = Comment.findAll(By(Comment.post, postId.toLong))
      //reRender
    }
  }

  def render = {
    ".comment-count *" #> (comments.length + " comments") &
      ".comment" #> comments.map {
        c =>
          ".comment-author-info *" #> (c.author.get match {
            case "" => "guest"
            case _ => c.author.get
          }) &
            ".comment-date *" #> YabeHelper.fmtDateStr(c.postedAt.get) &
            ".comment-content-span" #> Unparsed(c.content.get.replaceAll("\n", "<br />"))
      }
  }

  /**
   * This could suffice for comments:
   * CometListerner.listenerFor(Full(postId)) match {
   *  	case a: LiftActor => {
   *  		info(post)
   *  		a ! Message(postId, comments)
   *  	}
   *  	case _ => info("No actor to send an update")
   * }
   */
//  def post(x: Any): JsCmd = {
//
//    val (postId: String, post) = x match {
//      case m: Map[String, String] => (
//        m.get("postId").getOrElse("No comet name"),
//        m.get("post").getOrElse("1"))
//      case _ => ("No comet name", "1")
//    }
//
//    CometListerner.listenerFor(Full(postId)) match {
//      case a: LiftActor => {
//        info(post)
//        a ! Message(postId, comments)
//      }
//      case _ => info("No actor to send an update")
//    }
//    JsCmds.Noop
//  }

}