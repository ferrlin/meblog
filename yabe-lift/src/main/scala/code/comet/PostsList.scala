package code.comet

import scala.xml.Unparsed
import net.liftweb._
import http._
import util._
import Helpers._
import code.model._
import code.lib._
import MapperBinder._

class PostsList extends CometActor with CometListener {
  private var posts = List[Post]()

  def registerWith = PostsServer

  override def lowPriority = {
    case p: List[Post] => posts = p; reRender
  }

  def render = {
    "*" #> posts.map {
      p =>
        "*" #> bindMapper(p, {
          ".post-comments *" #> (" | " + p.countComments + " comments " + p.latestCommentAuthor)
        }) _
    }
  }
}